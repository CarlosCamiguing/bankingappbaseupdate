﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApp
{
    class Account
    {
        private int _acctNo;

        private string _acctHolderName;

        protected double _balance;

        protected float _annualIntrRate;



        public Account(int acctNo, string acctHolderName)
        {
            _acctNo = acctNo;
            _acctHolderName = acctHolderName;
            _balance = 0;
            _annualIntrRate = 0;


        }

        public int AccountNumber
        {
            get
            {
                return _acctNo;
            }
        }
        public string AcctHolderName
        {
            get
            {
                return _acctHolderName;
            }
            set
            {
                _acctHolderName = value;
            }
        }

        public virtual float AnnualIntRate
        {
            get
            {
                return _annualIntrRate;
            }
            set
            {
                _annualIntrRate = value;
            }
        }

        public float MonthlyIntrRate
        {
            get
            {
                return _annualIntrRate / 12;
            }
            set
            {
                _annualIntrRate = value;
            }
        }

        public double Balance
        {
            get
            {
                return _balance;
            }
        }

        public double Deposit(double amount)
        {//TODO: implement deposit
            return 0;
        }

        public double Withdraw(double amount)
        {//TODO: implement withdraw
            return 0;
        }

        public void Load(StreamReader aactFileReader)
        {

        }

        public void Save(StreamWriter acctFileWriter)
        {

        }

        public override string ToString()
        {
            return $"Account for {_acctHolderName} with balance {_balance}";
        }
    }
}
