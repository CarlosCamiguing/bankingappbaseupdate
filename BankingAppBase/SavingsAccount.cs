﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApp
{
    class SavingsAccount : Account
    {
        private const double MATCHING_DEPOSIT_RATIO = 0.5;

        private const float MIN_INTEREST_RATE = 3.0f;

        public SavingsAccount(int acctNo, string acctHolderName) : base (acctNo, acctHolderName)
        {
           
        }

        public override float AnnualIntRate
        {
            get
            {
                return _annualIntrRate;
            }
        }

        public double Deposit(double amount)
        {
            return 0;
        }

    }
}
