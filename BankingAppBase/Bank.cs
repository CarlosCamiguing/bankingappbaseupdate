﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApp
{
    enum AccountType
    {
        Chequings = 1,
        Savings

    }
    class Bank
    {
        private List<Account> _accountList;

        public Bank()
        {
            _accountList = new List<Account>();
        }

        public IEnumerable<Account> Accounts
        {
            get => (IEnumerable < Account >) ((IEnumerable<Account>)_accountList).GetEnumerator();
        }

        public Account OpenAccount(string clientName, AccountType acctType, int acctNo)
        {
            //depending on account type create a savings account or a checkings account
            Account newAccount;
            switch (acctType)
            {
                case AccountType.Chequings:
                    newAccount = new ChequingAccount(acctNo, clientName);
                    break;

                case AccountType.Savings:
                    newAccount = new SavingsAccount(acctNo, clientName);
                    break;

                default:
                    newAccount = new Account(acctNo, clientName);
                    break;
            
            }
            return null;
        }
    }
}
