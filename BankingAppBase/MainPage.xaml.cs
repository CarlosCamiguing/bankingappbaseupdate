﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace BankingApp
{
    
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Bank _bank;

        public MainPage()
        {
            this.InitializeComponent();

            //create a new bank
            _bank = new Bank();
        }

        private void OnCreateAccount(object sender, RoutedEventArgs e)
        {
            //obtain the account data from the user via controls
            int acctNo = int.Parse(_txtAcctNo.Text);
            string acctHolderName = _txtAcctHolder.Text;
            AccountType acctType = (AccountType)_cmbAcctType.SelectedIndex;
            float annualIntRate = float.Parse(_txtIntrRate.Text);
            double balance = double.Parse(_txtInitBalance.Text);


            //open the account with the bank object

            Account acct = _bank.OpenAccount(acctHolderName, acctType, acctNo);
            acct.AnnualIntRate = annualIntRate;
            acct.Deposit(balance);

            //refresh the list of accounts displayed in the UI

            _lstAccountList.Items.Add(acct);
        }
    }
}
