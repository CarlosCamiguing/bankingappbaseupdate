﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApp
{
    class ChequingAccount : Account
    {
        private const double OVERDRAFT_LIMIT = 500.0;

        private const float MAX_INTEREST_RATE = 1.0f;


        public ChequingAccount(int acctNo, string acctHolderName) : base (acctNo, acctHolderName)
        {

        }

        public override float AnnualIntRate
        {
            get
            {
                return _annualIntrRate;
            }
               
        }


        public double Withdraw(double amount)
        {
            return 0;
        }


    }
}
